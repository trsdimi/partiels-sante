# MINEURS ⛏

## Planning 📅

| Mercredi                          | Horaires     | Documentation                | Moodle |
|:----------------------------------|:------------:|:----------------------------:|:------:|
| **EPICHAIN** - HIRON              | 10h - 12h    | ✔️ Tous docs. + calculatrice | ✔️    |
| **EPIQUANTI** - EZRATTY           | 9h30 - 10h15 | ❌                           | ✔️    |
| **EPIGOOD** - PARAMALINGAM        | 10h30 - 12h  | ❌                           | ✔️    |
| **EPIFIN (PROFI/RISBA)** - LANIER | 9h - 10h30   | ✔️ Tous docs. cours + notes  | ❌    |
| **EPIFIN (MAFI3)** - GROSCOT      | 11h - 12h    | ✔️ Tous docs. + calculatrice | ✔️    |

## Infos 💖


## Links 🔗