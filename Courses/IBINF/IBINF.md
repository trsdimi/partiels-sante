# Bio-Info 🍃

## Infos 💖
L'exam est ez, car 23 des 26 questions viennent de la [banque de questions](https://gitlab.com/NonoLeLion/partiels-sante/-/blob/main/Courses/IBINF/ressources/Questions%20database.pdf).  
Donc en gros il faut juste apprendre les questions de la **banque de questions** par ❤ puis s'entraîner sur le [quiz d'entraînement](https://moodle.cri.epita.fr/mod/quiz/view.php?id=26413)

### Déroulement de l'examen 📚
26 questions au total, avec:
 - 22 questions (figurant dans la **banque de questions**)
 - 3 questions
 - 1 question de synthèse (figurant dans la **banque de questions**)

*NB: Les questions de QCM sont à points négatifs.*

## Links 🔗

[Moodle](https://moodle.cri.epita.fr/course/view.php?id=1312)  
[Banque de questions](https://gitlab.com/NonoLeLion/partiels-sante/-/blob/main/Courses/IBINF/ressources/Questions%20database.pdf) ⭐  
[Quiz d'entraînement](https://moodle.cri.epita.fr/mod/quiz/view.php?id=26413) ⭐  
[Quiz d'entraînement (synthèse)](https://moodle.cri.epita.fr/mod/quiz/view.php?id=26415) ⭐  
[100 questions d'entraînement](https://gitlab.com/NonoLeLion/partiels-sante/-/blob/main/Courses/IBINF/ressources/100%20questions.pdf) ⭐  
[Les slides du cours](https://gitlab.com/NonoLeLion/partiels-sante/-/blob/main/Courses/IBINF/ressources/slides)  
[Liste de modifications post-traductionnelles chez les protéines](https://moodle.cri.epita.fr/mod/url/view.php?id=26394)
