Require Import List.
Import ListNotations.

Inductive color : Type := White | Black.

Proposition not_same : White = Black -> False.
  intro.
  discriminate H.
Qed.

Proposition same : forall c1 c2 c3:color, (c1,c2) = (c3,c3) -> c1=c2.
Proof.
  intros.
  injection H.
  intros.
  rewrite H0, H1.
  reflexivity.
Qed.

Fixpoint app (A: Type) (l m: list A) : list A :=
  match l with
  | [] => m
  | a :: l1 => a :: app A l1 m
  end.

Proposition concat_nil_left:
  forall (A:Set) (l: list A),
    ([]++l) = l.
Proof.
  intros.
  simpl.
  reflexivity.
Qed.

Proposition concat_nil_right:
  forall (A:Set) (l: list A),
    (l++[]) = l.
Proof.
  intros.
  induction l.
  - simpl.
    reflexivity.
  - simpl.
    rewrite IHl.
    reflexivity.
Qed.
