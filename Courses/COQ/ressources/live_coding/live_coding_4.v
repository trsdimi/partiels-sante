Inductive even : nat -> Prop :=
| even0 : even 0
| evenS : forall p:nat, even p -> even (S (S p)).

Check even_ind.

Lemma four_even : even 4.
Proof.
  apply evenS.
  apply evenS.
  apply even0.
Qed.

Require Import Arith.

Lemma even_double :
  forall n, even n -> exists k, n = k * 2.
Proof.
  intros n H.
  induction H.
  - exists 0.
    simpl.
    reflexivity.
  - destruct IHeven.
    exists (S x).
    rewrite H0.
    simpl.
    reflexivity.
Qed.

Lemma double_even :
  forall n, even (n * 2).
Proof.
  intros.
  induction n.
  - simpl.
    apply even0.
  - simpl.
    apply evenS.
    assumption.
Qed.


Require Import Bool.
Require Import Coq.Arith.PeanoNat.

Section BSTrees.
Definition key := nat.
Variable V : Type.

Inductive tree : Type :=
| E : tree
| T : tree -> key -> V -> tree -> tree.

Fixpoint ForallT (P: key -> Prop) (t: tree) : Prop :=
  match t with
  | E => True
  | T l k v r => P k /\ ForallT P l /\ ForallT P r
  end.

Inductive BST : tree -> Prop :=
| BST_E : BST E
| BST_T : forall l x v r,
    ForallT (fun y => y < x) l ->
    ForallT (fun y => y > x) r ->
    BST l ->
    BST r ->
    BST (T l x v r).

Fixpoint insert (x : key) (v : V) (t : tree) : tree :=
  match t with
  | E => T E x v E
  | T l y v' r => if x <? y then T (insert x v l) y v' r
                  else if y <? x then T l y v' (insert x v r)
                       else T l x v r
  end.

(* insert preserves any node predicate *)
Lemma ForallT_insert : forall (P : key -> Prop) (t : tree)
                              (k : key) (v : V),
    ForallT P t ->
      P k -> ForallT P (insert k v t).
Proof.
  intros.
  induction t; simpl.
  - repeat split; assumption.
  - destruct (k <? k0); simpl in *; intuition.
    destruct (k0 <? k); simpl in *; intros; intuition.
Qed.

Proposition insert_Empty : forall k v, BST (insert k v E).
Proof.
  intros.
  simpl.
  apply BST_T; simpl.
  - reflexivity.
  - reflexivity.
  - apply BST_E.
  - apply BST_E.
Qed.


Lemma not_lt_nor_gt_is_eq : forall x k,
  (x <? k) = false -> (k <? x) = false <-> x = k.
Proof.
  intros.
  split; intro.
  - apply Nat.ltb_ge in H0.
    rewrite Nat.le_lteq in H0.
    destruct H0.
    * apply Nat.ltb_nlt in H.
      contradiction.
    * assumption.
  - rewrite Nat.ltb_ge.
    rewrite Nat.le_lteq.
    right.
    assumption.
Qed.

(* insert preserves the BST property *)
Theorem insert_BST : forall (k : key) (v : V) (t : tree),
    BST t -> BST (insert k v t).
Proof.
  intros.
  induction H.
  - apply insert_Empty.
  - simpl.
    case_eq (k <? x).
    * intro.
      apply BST_T.
      ** apply ForallT_insert.
         *** assumption.
         *** Search ( _ <? _).
             rewrite Nat.ltb_lt in H3.
             assumption.
      ** assumption.
      ** assumption.
      ** assumption.
    * case_eq (x <? k).
      ** intros.
         apply BST_T; try assumption.
         apply ForallT_insert.
         *** assumption.
         *** rewrite Nat.ltb_lt in H3.
             assumption.
      ** intros.
         assert (x = k).
         *** apply not_lt_nor_gt_is_eq; assumption.
         *** apply BST_T; try assumption.
             rewrite <- H5.
             assumption.
             rewrite <- H5.
             assumption.
Qed.


End BSTrees.
