Proposition three_step :
  forall (P: nat -> Prop),
    P 0 -> P 1 -> P 2 ->
    (forall n, P n -> P (S (S (S n)))) ->
    forall x, P (x).
Proof.
  admit.
Admitted.

Proposition all_rugby_score_are_possible:
  forall x, exists a b, x+8 = a*3 + b*5.
Proof.
  intros.
  induction x using three_step.
  - exists 1,1.
    simpl.
    reflexivity.
  - exists 3,0.
    simpl.
    reflexivity.
  - exists 0,2.
    simpl.
    reflexivity.
  - destruct IHx.
    destruct H.
    exists (S x0),x1.
    simpl.
    Search (S _ = S _).
    repeat apply eq_S.
    rewrite <- H.
    reflexivity.
Qed.
