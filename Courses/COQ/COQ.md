# COQ 🐔

## Infos 💖


## Links 🔗

[Moodle](https://moodle.epita.fr/course/view.php?id=2463) ⭐  
[Cheat Sheet](https://gitlab.com/NonoLeLion/partiels-sante/-/blob/main/Courses/COQ/ressources/cheat_sheet.pdf) ⭐  
[TPs](https://gitlab.com/NonoLeLion/partiels-sante/-/blob/main/Courses/COQ/ressources/TPs) ⭐  
[Live coding](https://gitlab.com/NonoLeLion/partiels-sante/-/blob/main/Courses/COQ/ressources/live_coding) ⭐  
[Exam blanc](https://gitlab.com/NonoLeLion/partiels-sante/-/blob/main/Courses/COQ/ressources/exam_blanc.v) ⭐  
[Cours](https://gitlab.com/NonoLeLion/partiels-sante/-/blob/main/Courses/COQ/ressources/cours)  