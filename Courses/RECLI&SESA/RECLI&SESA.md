# Recherche Clinique et Informatique 🖨

## Infos 💖

## Links 🔗

[Moodle](https://moodle.epita.fr/course/view.php?id=2214)  
[Slides](https://gitlab.com/NonoLeLion/partiels-sante/-/tree/main/Courses/RECLI&SESA/ressources/slides) ⭐  

# Sécurité / Sécurité & Santé 🔒

## Infos 💖

## Links 🔗

[Moodle](https://moodle.epita.fr/course/view.php?id=2208)  