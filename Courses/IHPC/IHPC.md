# Introduction au HPC 🧬

## Infos 💖

Le cours de HCP porte sur [ce jeu de slides](https://gitlab.com/NonoLeLion/partiels-sante/-/blob/main/Courses/IHCP/ressources/slides/DML-HPC_Recap.pdf)  
*NB : Les slides marquées d'un ⚠️ sont les potentielement les slides plus importantes !*

## Links 🔗

[Moodle](https://moodle.cri.epita.fr/course/view.php?id=1295)  
[Le "cours"](https://gitlab.com/NonoLeLion/partiels-sante/-/blob/main/Courses/IHPC/ressources/slides/DML-HPC_Recap.pdf) ⭐  
[Les vidéos](https://epitafr.sharepoint.com/:f:/r/sites/2024SANTE/Documents%20partages/General/Recordings/IHPC?csf=1&web=1&e=vEw3LP) ⭐  