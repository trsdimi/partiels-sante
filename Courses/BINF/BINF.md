# Bio-Informatique 🍃

## Infos 💖


## Links 🔗

[Moodle](https://moodle.cri.epita.fr/course/view.php?id=1309)  
[Slides](https://gitlab.com/NonoLeLion/partiels-sante/-/blob/main/Courses/BINF/ressources/slides) ⭐  
[Les vidéos](https://epitafr.sharepoint.com/:f:/r/sites/2024SANTE/Documents%20partages/General/Recordings/Bio-Info%20(Parruto)?csf=1&web=1&e=SGlgX5) ⭐  