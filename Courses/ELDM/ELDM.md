# Eléments de langage du domaine médical 💊

## Infos 💖

## Links 🔗

[Moodle](https://moodle.cri.epita.fr/course/view.php?id=1290)  
[Les slides](https://gitlab.com/NonoLeLion/partiels-sante/-/blob/main/Courses/ELDM/ressources/slides) ⭐  
[Les vidéos](https://epitafr.sharepoint.com/:f:/r/sites/2024SANTE/Documents%20partages/General/Recordings/%C3%89l%C3%A9ments%20de%20Langage%20M%C3%A9dical?csf=1&web=1&e=UdjFrG) ⭐  