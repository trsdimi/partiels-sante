# Partiels SANTE 💖

Les liens avec des ⭐ sont les liens conseiller pour réviser.

## Planning 📅

### Examen Janvier *(semaine du 08/01/2024)* 🦍

| Mardi                                                                                              | Horaires      | Documentation    | Moodle |
|:---------------------------------------------------------------------------------------------------|:-------------:|:----------------:|:------:|
| [COQ](https://gitlab.com/NonoLeLion/partiels-sante/-/blob/main/Courses/COQ/COQ.md) -  ZIAT         | 9h30 - 11h30  | ❓               | ✔️    |
| [DR9](https://gitlab.com/NonoLeLion/partiels-sante/-/blob/main/Courses/DR9/DR9.md) - VAURY         | 11h45 - 12h45 | ❓               | ✔️    |
| [GRS](https://gitlab.com/NonoLeLion/partiels-sante/-/blob/main/Courses/GRS/GRS.md) - DANIEL        | 14h - 15h10   | Tout autorisé    | ✔️    |

| Mercredi                                                                                           | Horaires      | Documentation | Moodle |
|:---------------------------------------------------------------------------------------------------|:-------------:|:-------------:|:------:|
| [MINEURS](https://gitlab.com/NonoLeLion/partiels-sante/-/blob/main/Courses/MINEURS/MINEURS.md)     | 9h - 12h      |               |        |
| **SISA** - DE BELENE      | 13h - 14h      | ❌                               | ✔️    |
| **IMSY** - LEMOINE        | 14h05 - 15h05  | ✔️ Docs. de cours + calculatrice | ✔️    |
| **H360/PMSI** - DANIEL    | 15h10 - 16h10  | ✔️ Docs. de cours + calculatrice | ✔️    |
| [RECLI/SESA](https://gitlab.com/NonoLeLion/partiels-sante/-/blob/main/Courses/RECLI&SESA/RECLI&SESA.md) - DANIEL | 16h10 - 17h10  | ✔️ Docs. de cours + calculatrice | ✔️    |
| [BIOINF_A](https://gitlab.com/NonoLeLion/partiels-sante/-/blob/main/Courses/BIOINF_A/BIOINF_A.md) - PARUTTO      | 17h15 - 18h45  | ❌                               | ✔️    |
| **BIOET** - GRUSON        | 18h50 - 19h50  | ❓                                | ✔️    |

| Jeudi                                   | Horaires   | Moodle         |
|:----------------------------------------|:----------:|:--------------:|
| **Présentation consignes de stage FE**  | 19h/19h30  | ✔️ (Distanciel)|

---

### Examen Juin *(semaine du 26/06/2023)* 👀

| Mardi                                                                                                   | Horaires      | Documentation    | Moodle |
|:--------------------------------------------------------------------------------------------------------|:-------------:|:----------------:|:------:|
| [MPRO](https://gitlab.com/NonoLeLion/partiels-sante/-/blob/main/Courses/MPRO/MPRO.md) - ROUSSENQ/GUELIN | 10h30 - 11h30 | Cours sur moodle | ✔️    |
| [BINF](https://gitlab.com/NonoLeLion/partiels-sante/-/blob/main/Courses/BINF/BINF.md) - PARUTTO         | 14h - 15h30   | ❌               | ✔️    |
| [ISDM](https://gitlab.com/NonoLeLion/partiels-sante/-/blob/main/Courses/ISDM/ISDM.md) - DANIEL          | 15h45 - 16h45 | Tout autorisé    | ✔️    |

| Mercredi                                                                                           | Horaires      | Documentation | Moodle |
|:---------------------------------------------------------------------------------------------------|:-------------:|:-------------:|:------:|
| [IHPC](https://gitlab.com/NonoLeLion/partiels-sante/-/blob/main/Courses/IHPC/IHPC.md) - DEMICHEL   | 10h30 - 12h   | Tout autorisé | ✔️     |

---

### Plus tard ? 💩

| ?                                                                                                  | Horaires      | Documentation | Moodle |
|:---------------------------------------------------------------------------------------------------|:-------------:|:-------------:|:------:|
| [ELDM](https://gitlab.com/NonoLeLion/partiels-sante/-/blob/main/Courses/ELDM/ELDM.md) - Dr RICHARD | ❓            | ❓            | ✔️     |
| [IMED](https://gitlab.com/NonoLeLion/partiels-sante/-/blob/main/Courses/IMED/IMED.md) - GERMAIN    | ❓            | ❓            | ✔️     |

---

### Mid partiels *(semaine du 24/04/2023)* 😴

| Lundi                                                                                           | Horaires     | Docu. + Calculatrice | Moodle |
|:------------------------------------------------------------------------------------------------|:------------:|:--------------------:|:------:|
| [PBS2](https://gitlab.com/NonoLeLion/partiels-sante/-/blob/main/Courses/PBS2/PBS2.md) - GROSCOT | 14h - 15h30  | ✔️                  | ✔️     |
| [PBS2](https://gitlab.com/NonoLeLion/partiels-sante/-/blob/main/Courses/PBS2/PBS2.md) - HEMON   | 15h45 - 17h15| ✔️                  | ✔️     |

| Mardi                                                                                                  | Horaires     | Documentation | Moodle |
|:-------------------------------------------------------------------------------------------------------|:------------:|:-------------:|:------:|
| [DBRE](https://gitlab.com/NonoLeLion/partiels-sante/-/blob/main/Courses/DBRE/DBRE.md) - MOIN           | 9h15 - 10h45 | ✔️           | ✔️     |
| [COIN](https://gitlab.com/NonoLeLion/partiels-sante/-/blob/main/Courses/COIN/COIN.md) (MANC) - DEWILDE | 11h - 12h30  | ❓            | ✔️     |

| Mercredi                                                                                             | Horaires     | Documentation | Moodle |
|:-----------------------------------------------------------------------------------------------------|:------------:|:-------------:|:------:|
| [IBINF](https://gitlab.com/NonoLeLion/partiels-sante/-/blob/main/Courses/IBINF/IBINF.md) - CRUZEL    | 9h30 - 10h30 | ❓            | ✔️     |
| [SYSBP](https://gitlab.com/NonoLeLion/partiels-sante/-/blob/main/Courses/SYSBP/SYSBP.md) - CHABRERIE | 10h45 - 12h15| ❓            | ✔️     |
